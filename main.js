// Variáveis var, let, const e typeof string, number e boolean;
/* 
let algumValor = 9;
console.log(algumValor);
console.log(typeof algumValor);
algumValor = "Paulo";
console.log(algumValor);
console.log(typeof algumValor);
let isIgual = 10 == 10;
console.log(isIgual);
console.log(typeof isIgual);
*/

// Operadores (=, +, -, *, /, %, Math.sqrt(), <, >, >=, ==, !=, ||, &&, ++);
// notaFinal = (nota1 + nota2) / 2;  --> 10
/* 
const valorX = 9;
const raiz = Math.sqrt(valorX);
console.log(raiz);

let nota1 = 8;
let nota2 = 3;
let notaFinal = (nota1 + nota2) / 2;
console.log(notaFinal);
*/

// Estruturas de decisão if/else, ? : e switch;
/*  
if (notaFinal >= 6){
    console.log("Aprovado!");
} else {
    console.log("Reprovado!");
}
console.log(notaFinal >= 6 ? "Aprovado!" : "Reprovado!");
*/

// Vetores e estruturas de repetição while, do/while, for, for/in e for/of;
/* let vetor = [7, 4, 6];

for(valor of vetor){
    console.log(valor);
}
 */

/* 
for(index in vetor){
    let valor = vetor[index];
    console.log(valor);
}
 */
/* for(let i = 0; i < vetor.length; i++){
    let valor = vetor[i];
    console.log(valor);
} */

/*
let index = 0;
while(index < vetor.length){
    let valor = vetor[index];
    console.log(valor);
    index++;
}
 */


// Funções tradicional e seta (=>);
/* function somar(a, b){
    return a + b;
}

let subtrair = (a, b) => a - b;
let resultaSubtracao = subtrair(10, 5);
console.log(resultaSubtracao); */

//let resultadoSoma = somar(10, 5);
//console.log(resultadoSoma);


// Módulos: module.exports = {} e require("...").
/* const calc = require("./calculadora");

console.log(   calc.somar(10, 5)        );
console.log(   calc.subtrair(10, 5)     );
console.log(   calc.multiplicar(10, 5)  );
console.log(   calc.dividir(10, 5)      ); */

const prompt = require("prompt-sync")();
let valor = prompt("Digite um valor: ");
console.log("Valor digitado: " + valor);
